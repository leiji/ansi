ansi
====

A small go library to control your terminal with ANSI sequences. Basic movement and colors are implemented.

It also provides helper functions to format your strings with SGR codes: `@!foo@~` becomes **foo**, `@{!_}bar@~` results in **_bar_**.

If you need further terminal control, see `termcap(3)`, `termcap(5)`,`terminfo(5)` and of course `curses(3)`, as this is way beyond the scope of this little library.

License (ISC)
-------------

	Copyright (c) 2013, Alexander Kahl

	Permission to use, copy, modify, and/or distribute this software for any
	purpose with or without fee is hereby granted, provided that the above
	copyright notice and this permission notice appear in all copies.

	THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
	WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
	MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
	ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
	ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
	OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


