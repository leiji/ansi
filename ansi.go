// Control your terminal with ansi sequences
package ansi

import (
	"fmt"
	"strings"
	"bytes"
)

const (
	ESC = "\033"
	CSI = ESC + "["
)

// Cursor Up
func CUU(n int) string {
	return fmt.Sprintf("%s%d%s", CSI, n, "A")
}

// Cursor Down
func CUD(n int) string {
	return fmt.Sprintf("%s%d%s", CSI, n, "B")
}

// Cursor Forward
func CUF(n int) string {
	return fmt.Sprintf("%s%d%s", CSI, n, "C")
}

// Cursor Back
func CUB(n int) string {
	return fmt.Sprintf("%s%d%s", CSI, n, "D")
}

// Cursor Next Line
func CNL(n int) string {
	return fmt.Sprintf("%s%d%s", CSI, n, "E")
}

// Cursor Previous Line
func CPL(n int) string {
	return fmt.Sprintf("%s%d%s", CSI, n, "F")
}

// Cursor Horizontal Absolute
func CHA(n int) string {
	return fmt.Sprintf("%s%d%s", CSI, n, "G")
}

// Cursor Position (row, column) (1 based, 1;1 is the top left corner)
func CUP(row, column int) string {
	return fmt.Sprintf("%s%d;%d%s", CSI, row, column, "H")
}

// Erase Display
// n = 0: clear from cursor to end of screen
// n = 1: clear from cursor to beginning of screen
// n = 2: clear entire screen (and under DOS move to 1;1)
func ED(n int) string {
	return fmt.Sprintf("%s%d%s", CSI, n, "J")
}

// Erase in Line
// n = 0: clear from cursor to end of line
// n = 1: clear from cursor to beginning of line
// n = 2: clear entire line
// Cursor positon does not change
func EL(n int) string {
	return fmt.Sprintf("%s%d%s", CSI, n, "K")
}

// Scroll up
func SU(n int) string {
	return fmt.Sprintf("%s%d%s", CSI, n, "S")
}

// Scroll down
func SD(n int) string {
	return fmt.Sprintf("%s%d%s", CSI, n, "T")
}

// Horizontal and Vertical Position (same as CUP)
func HVP(row, column int) string {
	return fmt.Sprintf("%s%d;%d%s", CSI, row, column, "f")
}

// Select Graphic Rendition
func SGR(n ...int) string {
	var ns []string
	for _, c := range n {
		ns = append(ns, fmt.Sprintf("%d", c))
	}
	s := strings.Join(ns, ";")
	return fmt.Sprintf("%s%s%s", CSI, s, "m")
}

// Device Status Report
func DSR() string {
	return CSI + "6n"
}

// Save Cursor Position
func SCP() string {
	return CSI + "s"
}

// Restore Cursor Position
func RCP() string {
	return CSI + "u"
}

// DEC text cursor enable mode (VT-300)
// show or hide the cursor
func DECTCEM(show bool) string {
	m := "l" // default: hide
	if show {
		m = "h"
	}
	return CSI + "?25" + m
}

// SGR Codes:

const (
	SGR_RESET                    = 0
	SGR_BOLD                     = 1
	SGR_FAINT                    = 2
	SGR_ITALIC_ON                = 3
	SGR_UNDERLINE                = 4
	SGR_BLINK_SLOW               = 5
	SGR_BLINK_RAPID              = 6
	SGR_IMAGE_NEGATIVE           = 7
	SGR_REVERSE                  = SGR_IMAGE_NEGATIVE
	SGR_CONCEAL                  = 8
	SGR_CROSSED_OUT              = 9
	SGR_PRIMARY_FONT             = 10
	SGR_ALTERNATE_FONT_1         = 11
	SGR_ALTERNATE_FONT_2         = 12
	SGR_ALTERNATE_FONT_3         = 13
	SGR_ALTERNATE_FONT_4         = 14
	SGR_ALTERNATE_FONT_5         = 15
	SGR_ALTERNATE_FONT_6         = 16
	SGR_ALTERNATE_FONT_7         = 17
	SGR_ALTERNATE_FONT_8         = 18
	SGR_ALTERNATE_FONT_9         = 19
	SGR_FRAKTUR                  = 20
	SGR_BOLD_OFF                 = 21
	SGR_UNDERLINE_DOUBLE         = SGR_BOLD_OFF
	SGR_NORMAL_INTENSITY         = 22
	SGR_NOT_ITALIC_NOT_FRAKTUR   = 23
	SGR_UNDERLINE_NONE           = 24
	SGR_BLINK_OFF                = 25
	SGR_RESERVED_26              = 26
	SGR_IMAGE_POSITIVE           = 27
	SGR_REVEAL                   = 28 // conceal off
	SGR_NOT_CROSSED_OUT          = 29
	SGR_FOREGROUND_BLACK         = 30
	SGR_FOREGROUND_RED           = 31
	SGR_FOREGROUND_GREEN         = 32
	SGR_FOREGROUND_YELLOW        = 33
	SGR_FOREGROUND_BLUE          = 34
	SGR_FOREGROUND_MAGENTA       = 35
	SGR_FOREGROUND_CYAN          = 36
	SGR_FOREGROUND_WHITE         = 37
	SGR_FOREGROUND_256           = 38
	SGR_FOREGROUND_DEFAULT       = 39
	SGR_BACKGROUND_BLACK         = 40
	SGR_BACKGROUND_RED           = 41
	SGR_BACKGROUND_GREEN         = 42
	SGR_BACKGROUND_YELLOW        = 43
	SGR_BACKGROUND_BLUE          = 44
	SGR_BACKGROUND_MAGENTA       = 45
	SGR_BACKGROUND_CYAN          = 46
	SGR_BACKGROUND_WHITE         = 47
	SGR_BACKGROUND_256           = 48
	SGR_BACKGROUND_DEFAULT       = 49
	SGR_RESERVED_50              = 50
	SGR_FRAMED                   = 51
	SGR_ENCIRCLED                = 52
	SGR_OVERLINED                = 53
	SGR_NOT_FRAMED_NOT_ENCIRCLED = 54
	SGR_NOT_OVERLINED            = 55
	SGR_RESERVED_56              = 56
	SGR_RESERVED_57              = 57
	SGR_RESERVED_58              = 58
	SGR_RESERVED_59              = 59
	// 60..109 ignored

)

var Short map[string]int = map[string]int{
	"~": SGR_RESET,
	"!": SGR_BOLD,
	"_": SGR_UNDERLINE,
	"%": SGR_IMAGE_NEGATIVE,
	"&": SGR_IMAGE_POSITIVE,
/*	"0": SGR_PRIMARY_FONT,
	"1": SGR_ALTERNATE_FONT_1,
	"2": SGR_ALTERNATE_FONT_2,
	"3": SGR_ALTERNATE_FONT_3,
	"4": SGR_ALTERNATE_FONT_4,
	"5": SGR_ALTERNATE_FONT_5,
	"6": SGR_ALTERNATE_FONT_6,
	"7": SGR_ALTERNATE_FONT_7,
	"8": SGR_ALTERNATE_FONT_8,
	"9": SGR_ALTERNATE_FONT_9, */
	".": SGR_NORMAL_INTENSITY,
	"k": SGR_FOREGROUND_BLACK,
	"r": SGR_FOREGROUND_RED,
	"g": SGR_FOREGROUND_GREEN,
	"y": SGR_FOREGROUND_YELLOW,
	"b": SGR_FOREGROUND_BLUE,
	"m": SGR_FOREGROUND_MAGENTA,
	"c": SGR_FOREGROUND_CYAN,
	"w": SGR_FOREGROUND_WHITE,
	"d": SGR_FOREGROUND_DEFAULT,
	"K": SGR_BACKGROUND_BLACK,
	"R": SGR_BACKGROUND_RED,
	"G": SGR_BACKGROUND_GREEN,
	"Y": SGR_BACKGROUND_YELLOW,
	"B": SGR_BACKGROUND_BLUE,
	"M": SGR_BACKGROUND_MAGENTA,
	"C": SGR_BACKGROUND_CYAN,
	"W": SGR_BACKGROUND_WHITE,
	"D": SGR_BACKGROUND_DEFAULT,
}

const ESC_RUNE = rune('@')

func replaceColor(s string) string {
	sr := strings.NewReader(s)
	res := bytes.Buffer{}
	LOOP: for {
		ch, _, err := sr.ReadRune()
		if err != nil {
			break LOOP
		}
		
		if ch == ESC_RUNE {
			// peek into the next rune
			next, _, err := sr.ReadRune()
			// was the last rune?
			if err != nil {
				res.WriteRune(ch)
				break LOOP
			}
			// no need to escape the @, unrecognised sequences are printed as the are
			// check the next rune
			// @ aborts, results in a single @
			// if next == ESC_RUNE {
			// 	res.WriteRune(ch)
			// 	continue LOOP
			// }
			// is it a compound color code?
			if next == rune('{') {

				var runes []rune = []rune{ch,next}

				colors := []int{}
				
				COMPOUND: for {
					r, _, e := sr.ReadRune()
					// end of string, nothing to escape here, just return
					if e != nil {
						res.WriteString(string(runes))
						break LOOP
					}
					// end of color definition
					if r == rune('}') {
						break COMPOUND
					}

					if color, ok := Short[string(r)] ; ok {
						colors = append(colors, color)
					}
					runes = append(runes, r)
				}
				res.WriteString(SGR(colors...))
				continue LOOP // handled the compound expression, continue with the stuff after it

			}

			// is the next rune a color code? -> SGR for this single char
			if c, ok := Short[string(next)] ; ok {
				res.WriteString(SGR(c))
			} else {
				res.WriteRune(ch)
				res.WriteRune(next)
			}
		} else {
			res.WriteRune(ch)
		}
	}

	return res.String()




}

func ReplaceColor(s string) string {
	return replaceColor(s)
}

func Printf(format string, v... interface{}) {
	fmt.Printf(replaceColor(format), v...)
}


func Print(v... interface{}) {
	s := fmt.Sprint(v...)
	fmt.Print(replaceColor(s))
}

func Println(v... interface{}) {

	s := fmt.Sprint(v...)
	fmt.Println(replaceColor(s))
}


func Sprintf(format string, v... interface{}) string {
	return fmt.Sprintf(replaceColor(format), v...)
}


func Sprint(v... interface{}) string {
	s := fmt.Sprint(v...)
	return replaceColor(s)
}

func Sprintln(v... interface{}) string {

	s := fmt.Sprintln(v...)
	return replaceColor(s)
}
