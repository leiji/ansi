package main

import (
	"bitbucket.org/leiji/ansi"
	"os"
	"fmt"
)

func main() {
	ansi.Println(os.Args)
	for k, _ := range ansi.Short {
		s := fmt.Sprintf("@%s%s written like this @~", k,k)
		fmt.Printf("%s ==> ", k)
		ansi.Println(s)
	}

}
